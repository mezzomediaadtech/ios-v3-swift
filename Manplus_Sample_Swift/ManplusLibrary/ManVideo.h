//
//  ManVideo.h
//  Manplus_v200
//
//  Created by MezzoMedia on 2019. 6. 24..
//  Copyright © 2019년 MezzoMedia. All rights reserved.
//

#ifndef ManVideo_h
#define ManVideo_h

#import <UIKit/UIKit.h>
#import "ManAdDefine.h"
#import "ManViewability.h"

#endif /* ManVideo_h */

@protocol ManVideoDelegate;

@interface ManVideo : UIView{
    NSMutableData *responseData;
}

/* 전달받는 뷰컨트롤러의 객체
 */
@property (nonatomic, assign) id<ManVideoDelegate>videoDelegate;

/* 광고를 사용하는 유저의 성별 정보
 남성: @"1"
 여성: @"2"
 */
@property (nonatomic, copy) NSString *gender;

/* 광고를 사용하는 유저의 나이 정보
 age : @"20"
 */
@property (nonatomic, copy) NSString *age;

/* 광고를 사용하는 유저의 매체ID
 userId : @"userId"
 */
@property (nonatomic, copy) NSString *userId;

/* 광고를 사용하는 유저의 매체ID
 userEmail : @"user@mezzomedia.co.kr"
 */
@property (nonatomic, copy) NSString *userEmail;

/* 광고를 사용하는 유저의 위치정보 제공 동의여부
 활용 미동의 : @"0"
 활용 동의 : @"1"
 */
@property (nonatomic, copy) NSString *userPositionAgree;

- (void)webviewSetting;

// Set app_id, app_name, stroe_url
- (void)appID:(NSString*)appID appName:(NSString*)appName storeURL:(NSString*)storeURL;

- (void)setSMS:(Boolean)sms setTel:(Boolean)tel setCalendar:(Boolean)calendar setStorePicture:(Boolean)storePicture setInlineVideo:(Boolean)inlineVideo;

- (void)apiData:(Boolean)api_data isAsset:(Boolean)is_asset;

// publisher ID, media ID, section ID, i_banner_w, i_banner_h
- (void)publisherID:(NSString*)publisherID mediaID:(NSString*)mediaID sectionID:(NSString*)sectionID x:(CGFloat)x y:(CGFloat)y width:(CGFloat)w height:(CGFloat)h;

- (void)autoplay:(bool)autoplay autoReplay:(bool)autoReplay muted:(bool)muted clickFull:(bool)clickFull closeBtnShow:(bool)closeBtnShow soundBtnShow:(bool)soundBtnShow clickBtnShow:(bool)clickBtnShow skipBtnShow:(bool)skipBtnShow clickVideoArea:(bool)clickVideoArea viewability:(bool)viewability;

// keyword parameter
- (void)keywordParam:(NSString*)param;

// external parameter
- (void)externalParam:(NSString*)param;

- (void)userAgeLevel:(NSString*)userAgeLevel;

- (void)startVideo;

- (void)stopVideo;

- (void)infoSDK;

- (NSString *)getAdType;

- (NSString *)getNativeResponse;

@end

/* 비디오 프로토콜
 */
@protocol ManVideoDelegate <NSObject>

/* 비디오 광고 수신 에러*/
- (void)didFailReceiveAd:(ManVideo*)adVideo errorType:(NSInteger)errorType;

/* 비디오 부정 재요청 (지정된 시간 이내에 재요청이 발생함) */
- (void)didBlockReloadAd:(ManVideo*)adBanner;

//- (NSMutableDictionary*)parseJsonData:(NSData*)adInfoData;

@end

