//
//  ViewController.swift
//  Manplus_Sample_Swift
//
//  Created by MezzoMedia on 19/08/2019.
//  Copyright © 2019 MezzoMedia. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var appID = ""              //Application ID
    var appName = ""            //Application Name
    var storeURL = ""           //App store URL
    
    var autoplay = true        //AutoPlay video ad
    var autoReplay = false      //Auto-replay video ad
    var muted = true           //Play video ad with muted
    var clickFull = true       //Set click area to land on ad details page
    var viewability = true     //Stop the video if the ad area appears to be less than 20% when scrolling through the page
    var closeBtnShow = true    //Close button clears ad area
    var soundBtnShow = true    //Button to control sound
    var clickBtnShow = true    //Ad click button
    var skipBtnShow = true     //Ad skip button
    var clickVideoArea = true  //Video area disappears when clicked
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setAppInfo()
        setVideoOpt()
        
        navigationItem.title = "Manplus"
    }

    // Set app_id, app_name, stroe_url
    func setAppInfo() {
        appID = "ApplicationID"
        appName = "ApplicationName"
        storeURL = "AppStoreURL"
    
    }
    
    // Set Video Option (Only video ad)
    /* =================================== */
    // Video Ad option
    // autoplay : AutoPlay video ad
    // autoReplay : Auto-replay video ad
    // muted : Play video ad with muted
    // clickFull : Set click area to land on ad details page
    // viewability : Stop the video if the ad area appears to be less than 20% when scrolling through the page
    // closeBtnShow : Close button clears ad area
    // soundBtnShow : Button to control sound
    // clickBtnShow : Ad click button
    // skipBtnShow : Ad skip button
    // clickVideoArea : Video area disappears when clicked (true : disappear)
    /* =================================== */

    func setVideoOpt() {
        autoplay = true
        autoReplay = true
        muted = true
        clickFull = true
        viewability = true
        closeBtnShow = false
        soundBtnShow = true
        clickBtnShow = true
        skipBtnShow = true
        clickVideoArea = false
        
    }
    
    // Ani : Sample App subview loading form

    // 띠배너
    // (Banner Ad)
    // Set Publisher ID, Media ID, Section ID, Xpoint, Ypoint, Banner Width, Banner Height, Target Area Width & Height, Type(banner or interstitial)
    /* =================================== */
    // Banner Ad view type
    // @“0” 띠 배너
    // @“1” 전면 배너 : 매체의 영역 소재내 위치 
    /* =================================== */
    
    @IBAction func setManBannerAd(_ sender: Any) {
        requestManBanner(withPid: "100", mid: "200", sid: "804817", ani: true, x: 0, y: 0, width: 414, height: 54, type: "0")
    }
    
    // 전면배너
    // (Interstitial Ad)
    // Set Publisher ID, Media ID, Section ID, viewType
    /* =================================== */
    // Interstitial Ad view style
    // @"0" 전체화면
    // @“1” 사이즈만 팝업(배경 투명)
    // @“2” 사이즈와 팝업(배경 alpha 0.8)
    /* =================================== */
    
    @IBAction func setManInterstitialAd(_ sender: Any) {
        requestManInterstitial(withPid: "100", mid: "200", sid: "804820", ani: false, viewType: "0")
    }
    
    // 동영상
    // (Video Ad)
    // Set Publisher ID, Media ID, Section ID
    @IBAction func setManVideoAd(_ sender: Any) {
        requestManVideo(withPid: "1075", mid: "30394", sid: "400884", ani: true, x: 0, y: 100, width: 414, height: 400)
    }
    
    func requestManBanner(withPid p: String?, mid m: String?, sid s: String?, ani isAni: Bool, x: CGFloat, y: CGFloat, width w: CGFloat, height h: CGFloat, type t: String?) {
        
        print("띠배너광고 호출")
        
        let manBannerController  = ManBannerController()
        manBannerController.setPID(p, mid: m, sid: s, x: x, y: y, width: w, height: h, type: t)
        manBannerController.setAppID(appID, appName: appName, storeURL: storeURL)
        navigationController?.pushViewController(manBannerController, animated: isAni)
    }

    func requestManInterstitial(withPid p: String?, mid m: String?, sid s: String?, ani isAni: Bool, viewType v: String?) {
        
        print("전면배너광고 호출")
        
        let manInterstitialController = ManInterstitialController()
        manInterstitialController.setPID(p, mid: m, sid: s, viewType: v)
        manInterstitialController.setAppID(appID, appName: appName, storeURL: storeURL)
        navigationController?.pushViewController(manInterstitialController, animated: isAni)
    }
    
    func requestManVideo(withPid p: String?, mid m: String?, sid s: String?, ani isAni: Bool, x: CGFloat, y: CGFloat, width w: CGFloat, height h: CGFloat) {
        
        print("동영상광고 호출")
        
        let manVideoController = ManVideoController()
        manVideoController.setPID(p, mid: m, sid: s, x: x, y: y, width: w, height: h)
        manVideoController.setAppID(appID, appName: appName, storeURL: storeURL)
        manVideoController.setAutoplay(autoplay, autoReplay: autoReplay, muted: muted, clickFull: clickFull, closeBtnShow: closeBtnShow, soundBtnShow: soundBtnShow, clickBtnShow: clickBtnShow, skipBtnShow: skipBtnShow, clickVideoArea: clickVideoArea, viewability: viewability)

//        // When there are two video areas (광고영역 두개 정의)
//        manVideoController.set2PID("102", mid: "202", sid: "803581", x: 0, y: 400, width: 400, height: 300)
//        manVideoController.set2AppID(appID, appName: appName, storeURL: storeURL)
//        manVideoController.set2Autoplay(autoplay, autoReplay: autoReplay, muted: muted, clickFull: clickFull, closeBtnShow: closeBtnShow, soundBtnShow: soundBtnShow, clickBtnShow: clickBtnShow, skipBtnShow: skipBtnShow, clickVideoArea: clickVideoArea, viewability: viewability)
        
        navigationController?.pushViewController(manVideoController, animated: isAni)
    }

}
