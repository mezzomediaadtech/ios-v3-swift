//
//  ManInterstitialController.swift
//  Manplus_Sample_Swift
//
//  Created by MezzoMedia on 19/08/2019.
//  Copyright © 2019 MezzoMedia. All rights reserved.
//

import Foundation
import WebKit

class ManInterstitialController: UIViewController, ManInterstitialDelegate {
    
    var manInterstitial: ManBanner?
    var publisherID = ""
    var mediaID = ""
    var sectionID = ""
    var viewType = ""
    
    var i_x: CGFloat = 0.0
    var i_y: CGFloat = 0.0
    var i_width: CGFloat = 0.0
    var i_height: CGFloat = 0.0

    var appID = ""
    var appName = ""
    var storeURL = ""
    
    func prefersStatusBarHidden() -> Bool {
        
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Interstitial Banner AD"
        view.backgroundColor = UIColor.white

        manInterstitial = ManBanner(frame: CGRect(x: i_x, y: i_y, width: self.view.frame.size.width, height: self.view.frame.size.height))
        
        manInterstitial?.infoSDK()

        manInterstitial?.gender = "2"
        manInterstitial?.age = "15"
        manInterstitial?.userId = "mezzomedia"
        manInterstitial?.userEmail = "mezzo@mezzomeida.co.kr"
        manInterstitial?.userPositionAgree = "1"
        
        manInterstitial?.interDelegate = self
        
        // u_age_level (0: 만13세 이하, 1: 만13세 이상)<권장 Parameter>
        manInterstitial?.userAgeLevel("1")
        
        // app_id, app_name, stroe_url 설정
        manInterstitial?.appID(appID, appName: appName, storeURL: storeURL)

        manInterstitial?.publisherID(publisherID, mediaID: mediaID, sectionID: sectionID, viewType: viewType)
        
        manInterstitial?.apiData(false, isAsset: false)
        
        // Keyword 타게팅을 위한 함수파라미터 (Optional)
        manInterstitial?.keywordParam("KeywordTargeting")
        
        manInterstitial?.externalParam("InterstitialAdditionalParameters")
        
        view.addSubview(manInterstitial!)
        
        manInterstitial?.startInterstitial()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
        print("ADInterstitial ================== didReceiveMemoryWarning")
    }
    
    func setPID(_ p: String?, mid m: String?, sid s: String?, viewType v: String?) {
        publisherID = p ?? ""
        mediaID = m ?? ""
        sectionID = s ?? ""
        viewType = v ?? ""
        
        print("ADInterstitial ================== Publisher, Media, Section NO")
        print("\(publisherID)")
        print("\(mediaID)")
        print("\(sectionID)")
        print("\(viewType)")
        print("======================================================")
    
    }
    
    func setAppID(_ app_id: String?, appName app_name: String?, storeURL store_url: String?) {
        appID = app_id ?? ""
        appName = app_name ?? ""
        storeURL = store_url ?? ""
        
        print("ADInterstitial ================== appID, appName, storeURL")
        print("\(appID)")
        print("\(appName)")
        print("\(storeURL)")
        print("======================================================")
        
    }
    
    func didFailReceiveAd(_ adBanner: ManBanner?, errorType: Int) {
        
        // errorType은 ManAdDefine.h 참조
        print(String(format: "[ManInterstitialAdViewController]=========== didFailReceiveInterstitial : errorType : %ld", errorType))
        var log = "none"
        switch errorType {
        case Int(NewManAdSuccess.rawValue):
            log = "성공"
            print("광고 타입[유료(guarantee) or 무료(house)] : \(String(describing: manInterstitial?.getAdType() ?? nil))")
        case Int(NewManAdClick.rawValue):
            log = "광고 클릭"
        case Int(NewManAdClose.rawValue):
            log = "광고 닫음"
        case Int(NewManAdNotError.rawValue):
            manInterstitial?.stopInterstitial()
            manInterstitial = nil;
            log = "광고 없음(No Ads)"
        case Int(NewManAdPassbackError.rawValue):
            log = "Sync 모드 필요 (패스백)"
        case Int(NewManAdTimeoutError.rawValue):
            log = "Timeout"
        case Int(NewManAdParsingError.rawValue):
            log = "파싱 에러"
        case Int(NewManAdDuplicateError.rawValue):
            log = "중복 호출 에러"
        case Int(NewManAdError.rawValue):
            log = "에러"
        case Int(NewManBrowserError.rawValue):
            log = "Browser Error"
        case Int(NewManAdNotExistError.rawValue):
            log = "존재하지 않는 요청 에러"
        case Int(NewManAdAppStoreUrlError.rawValue):
            log = "앱 Store URL 미입력"
        case Int(NewManAdIDError.rawValue):
            log = "사업자/미디어/섹션 코드 미존재"
        case Int(NewManAdTargetAreaError.rawValue):
            log = "광고영역크기 에러"
        case Int(NewManAdReloadTimeError.rawValue):
            log = "광고 재호출(Reload) 에러"
        case Int(NewManAdNetworkError.rawValue):
            log = "네트워크 에러"
        case Int(NewManAdFileError.rawValue):
            log = "광고물 파일 형식 에러"
        case Int(NewManAdCreativeError.rawValue):
            log = "광고물 요청 실패(Timeout)"
        default:
            break
        }
        
        log = log + (String(format: " : %ld", errorType))
        
        navigationController?.view.makeToast(log)
    }
    
    func didClickInterstitial() {
        // Click AD
        print("[ManInterstitialAdViewController]=========== Click Interstitial!")
    }
    
    func didCloseInterstitial() {
        // Close AD
        print("[ManInterstitialAdViewController]=========== Close Interstitial!")
    }
    
    // 지정된 주기 이내에 광고리로드가 발생됨
    func didBlockReloadAd(_ adBanner: ManBanner?) {
        print("[ManInterstitialAdViewController] =========== didBlockReloadAd")
    }
}
